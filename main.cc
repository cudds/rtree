#include <stdio.h>
#include <memory.h>
#include <fstream>
#ifdef WIN32
#include <crtdbg.h>
#endif //WIN32

#include "RTree/Core/RTree.h"
#include "Util/ConfigHelper.h"
#include "Util/StringUtil.h"
#include "DTranx/Util/Log.h"

//TODO: compile proto file so that no calling to Deploy script is required before compile
//because now protoc generate header files at Builder directory instead of include directory

/*
 * MemoryTest.cpp
 * This demonstrates a use of RTree
 */

// Use CRT Debug facility to dump memory leaks on app exit
#ifdef WIN32
// These two are for MSVS 2005 security consciousness until safe std lib funcs are available
#pragma warning(disable : 4996) // Deprecated functions
#define _CRT_SECURE_NO_DEPRECATE // Allow old unsecure standard library functions, Disable some 'warning C4996 - function was deprecated'

// The following macros set and clear, respectively, given bits
// of the C runtime library debug flag, as specified by a bitmask.
#ifdef   _DEBUG
#define  SET_CRT_DEBUG_FIELD(a) \
              _CrtSetDbgFlag((a) | _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG))
#define  CLEAR_CRT_DEBUG_FIELD(a) \
              _CrtSetDbgFlag(~(a) & _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG))
#else
#define  SET_CRT_DEBUG_FIELD(a)   ((void) 0)
#define  CLEAR_CRT_DEBUG_FIELD(a) ((void) 0)
#endif
#endif //WIN32

/*
 * Get a random float b/n two values
 * The returned value is >= min && < max (exclusive of max)
 * Note this is a low precision random value since it is generated from an int.
 */
static float RandFloat(float a_min, float a_max) {
	const float ooMax = 1.0f / (float) RAND_MAX;
	float retValue = ((float) rand() * ooMax * (a_max - a_min) + a_min);
	ASSERT(retValue >= a_min && retValue < a_max); // Paranoid check
	return retValue;
}

/*
 * Simplify handling of 3 dimensional coordinate
 */
struct Vec3 {
	Vec3() {
	}

	Vec3(float a_x, float a_y, float a_z) {
		v[0] = a_x;
		v[1] = a_y;
		v[2] = a_z;
	}

	Vec3 operator+(const Vec3& a_other) const {
		return Vec3(v[0] + a_other.v[0], v[1] + a_other.v[1], v[2] + a_other.v[2]);
	}

	void Print() {
		std::cout << "(" << v[0] << "," << v[1] << "," << v[2] << ")" << std::endl;
	}

	float v[3];
};

static bool BoxesIntersect(const Vec3& a_boxMinA, const Vec3& a_boxMaxA, const Vec3& a_boxMinB,
		const Vec3& a_boxMaxB) {
	for (int axis = 0; axis < 3; ++axis) {
		if (a_boxMinA.v[axis] > a_boxMaxB.v[axis] || a_boxMaxA.v[axis] < a_boxMinB.v[axis]) {
			return false;
		}
	}
	return true;
}

bool QueryResultCallback(RTree::Core::StringDataType a_data, void* a_context) {
	printf("search found %s\n", a_data.strData.c_str());
	return true;
}

std::vector<std::string> Split(std::string str, char delimiter) {
	size_t start = 0;
	size_t end = str.find_first_of(delimiter);
	std::vector<std::string> result;
	size_t size = str.size();
	if (size == 0) {
		return result;
	}
	if (end == std::string::npos) {
		result.push_back(str);
		return result;
	}
	while (end <= std::string::npos) {
		result.emplace_back(str.substr(start, end - start));
		if (end == std::string::npos || end == size - 1) {
			break;
		}
		start = end + 1;
		end = str.find_first_of(delimiter, start);
	}
	return result;
}

int main(int argc, char* argv[]) {
	DTranx::Util::Log::setLogPolicy( { { "Client", "PROFILE" }, { "Server", "PROFILE" }, { "Tranx",
			"PROFILE" }, { "Storage", "PROFILE" }, { "RPC", "PROFILE" }, { "Util", "PROFILE" }, {
			"Log", "PROFILE" } });
	if (argc < 4) {
		std::cout << "arg1: self address to bind to; arg2: self port to bind to"
				<< "arg3: first time to initialize rtree(0/1); arg4(optional): data set filename, used to initialize database"
				<< std::endl;
		exit(1);
	}
	std::string selfAddress(argv[1]);
	std::string selfPort(argv[2]);
	bool firstTime = (std::stoi(argv[3]) == 1);
	std::string inputFile = "";
	if (argc > 4) {
		inputFile = argv[4];
	}
	RTree::Util::ConfigHelper configHelper;
	configHelper.readFile("RTree.conf");
	std::string ipsStr = configHelper.read("IPS");
	std::vector<std::string> ips = RTree::Util::StringUtil::Split(ipsStr, ';');
	bool enableDebug = ("1" == configHelper.read("EnableDebug"));

	/*
	 * Number of objects in test set, must be > FRAC_OBJECTS for this test
	 */
	int InsertNum = std::stoi(configHelper.read("InsertNum"));
	int RemoveNum = std::stoi(configHelper.read("RemoveNum"));
	int SearchNum = std::stoi(configHelper.read("SearchNum"));
	const float MAX_WORLDSIZE = 1000000.0f;
	const float FRAC_WORLDSIZE = 10.0f;

	/*
	 * typedef the RTree usage just for convenience with iteration
	 */
	typedef RTree::Core::RTree<RTree::Core::StringDataType, float, 3, float, 30, 5> RTreeString;

	/*
	 * general purpose counter, declared here because MSVC 6 is not ansi compliant with 'for' loops.
	 */
	int index;
	/*
	 * Store objects in another container to test with, sized larger than we need
	 */
	std::pair<Vec3, Vec3> dataArray[InsertNum];

	/*
	 * Nullify array, size is known here
	 */
	memset(dataArray, 0, sizeof(dataArray));

	RTreeString tree(enableDebug, "tree.debug", true);
	tree.InitDTranxForRTree(30000, ips, selfAddress, std::stoul(selfPort.c_str(), nullptr, 10),
	true, firstTime, false);

	/*
	 * when inputFile is empty, randomly generate workloads based on RTree.conf
	 */
	if (inputFile.empty()) {

		/*
		 * Insertion
		 */
		for (index = 0; index < InsertNum; ++index) {
			RTree::Core::StringDataType newData(std::to_string(index));
			Vec3 min(RandFloat(-MAX_WORLDSIZE, MAX_WORLDSIZE),
					RandFloat(-MAX_WORLDSIZE, MAX_WORLDSIZE),
					RandFloat(-MAX_WORLDSIZE, MAX_WORLDSIZE));

			Vec3 extent = Vec3(RandFloat(0, FRAC_WORLDSIZE), RandFloat(0, FRAC_WORLDSIZE),
					RandFloat(0, FRAC_WORLDSIZE));
			Vec3 max = min + extent;
			dataArray[index] = std::make_pair(min, max);
			tree.Insert(min.v, max.v, newData);
			std::cout << "inserting element " << index << std::endl;
			min.Print();
			max.Print();
		}

		/*
		 * Search
		 */
		for (index = 0; index < SearchNum; ++index) {
			Vec3 searchMin(RandFloat(-MAX_WORLDSIZE, MAX_WORLDSIZE),
					RandFloat(-MAX_WORLDSIZE, MAX_WORLDSIZE),
					RandFloat(-MAX_WORLDSIZE, MAX_WORLDSIZE));
			Vec3 extent = Vec3(RandFloat(0, FRAC_WORLDSIZE), RandFloat(0, FRAC_WORLDSIZE),
					RandFloat(0, FRAC_WORLDSIZE));
			Vec3 searchMax = searchMin + extent;
			tree.Search(searchMin.v, searchMax.v, &QueryResultCallback, NULL);
		}

		if (RemoveNum > 0) {
			int numToStep = InsertNum / RemoveNum;
			/*
			 * Deletion
			 */
			for (index = 0; index < InsertNum; index += numToStep) {
				std::pair<Vec3, Vec3>& curData = dataArray[index];
				RTree::Core::StringDataType newData(std::to_string(index));
				tree.Remove(curData.first.v, curData.second.v, newData);
				printf("removing element %d\n", index);
				curData.first.Print();
				curData.second.Print();
			}
		}
	} else {
		std::ifstream ipFile(inputFile);
		if (!ipFile.is_open()) {
			VERBOSE("cannot open %s", inputFile.c_str());
			exit(0);
		}
		std::string line;
		float dimen1;
		float dimen2;
		float dimen3;
		RTree::Core::StringDataType data;
		while (std::getline(ipFile, line)) {
			std::vector<std::string> fields = Split(line, ',');
			if (fields.size() == 4) {
				dimen1 = stof(fields[2]);
				dimen2 = stof(fields[3]);
				dimen3 = stof(fields[1]);
				data = fields[0];
				Vec3 min(dimen1, dimen2, dimen3);
				tree.Insert(min.v, min.v, data);
			} else {
				VERBOSE("data format wrong");
				exit(0);
			}
		}
	}

#ifdef WIN32
	// Use CRT Debug facility to dump memory leaks on app exit
	SET_CRT_DEBUG_FIELD( _CRTDBG_LEAK_CHECK_DF );
#endif //WIN32

	return 0;
}

