/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef RTREE_RTREE_NODE_H_
#define RTREE_RTREE_NODE_H_

#include "RTree/Core/RTreeNode.pb.h"
#include "DTranxMempool.h"
#include "RTree/Util/LastNotCommitException.h"

namespace RTree {
namespace Core {

struct BasicDataType {
	BasicDataType() {

	}
	virtual ~BasicDataType() {

	}
	virtual void serialize(DataProto *dataProto) {

	}
	virtual void deserialize(const DataProto& dataProto) {

	}
	virtual bool operator==(const BasicDataType &other) const {
		return true;
	}
	virtual bool operator!=(const BasicDataType &other) const {
		return false;
	}
	virtual void CopyData(const BasicDataType& other) {
	}
};

struct StringDataType: BasicDataType {
	void serialize(DataProto *dataProto) {
		dataProto->set_data(strData);
	}
	void deserialize(const DataProto& dataProto) {
		strData = dataProto.data();
	}
	void CopyData(StringDataType& other) {
		other.strData = strData;
	}
	bool operator==(const StringDataType &other) const {
		return (strData == other.strData);
	}
	bool operator!=(const StringDataType &other) const {
		return (strData != other.strData);
	}
	StringDataType() {

	}
	StringDataType(std::string data) {
		strData = data;
	}
	StringDataType(const char* data) {
		strData = data;
	}
	void Print(std::stringstream& ss) {
		ss << strData << "\n";
	}
	std::string strData;
};

/*
 * Minimal bounding rectangle (n-dimensional)
 */
template<class ELEMTYPE, int NUMDIMS>
struct Rect {
	/*
	 *  Min/Max dimensions of bounding box
	 */
	ELEMTYPE m_min[NUMDIMS];
	ELEMTYPE m_max[NUMDIMS];
	void serialize(RectProto *rectProto) {
		for (int i = 0; i < NUMDIMS; ++i) {
			rectProto->add_min(m_min[i]);
			rectProto->add_max(m_max[i]);
		}
	}
	void deserialize(const RectProto& rectProto) {
		assert(rectProto.min_size() == NUMDIMS);
		assert(rectProto.max_size() == NUMDIMS);
		for (int i = 0; i < NUMDIMS; ++i) {
			m_min[i] = rectProto.min(i);
			m_max[i] = rectProto.max(i);
		}
	}

	void CopyRect(Rect& other) {
		for (int i = 0; i < NUMDIMS; ++i) {
			other.m_min[i] = m_min[i];
			other.m_max[i] = m_max[i];
		}
	}

	bool operator==(const Rect &other) const {
		for (int i = 0; i < NUMDIMS; ++i) {
			if (m_min[i] != other.m_min[i]) {
				return false;
			}
			if (m_max[i] != other.m_max[i]) {
				return false;
			}
		}
		return true;
	}
	bool operator!=(const Rect &other) const {
		for (int i = 0; i < NUMDIMS; ++i) {
			if (m_min[i] != other.m_min[i]) {
				return true;
			}
			if (m_max[i] != other.m_max[i]) {
				return true;
			}
		}
		return false;
	}
	void Print(std::stringstream& ss) {
		ss << "(";
		for (int i = 0; i < NUMDIMS - 1; ++i) {
			ss << m_min[i] << ",";
		}
		ss << m_min[NUMDIMS - 1] << "); ";
		ss << "(";
		for (int i = 0; i < NUMDIMS - 1; ++i) {
			ss << m_max[i] << ",";
		}
		ss << m_max[NUMDIMS - 1] << ")\n";
	}
	void CheckConsistency(std::string childKey) {
		for (int i = 0; i < NUMDIMS; ++i) {
			if (m_min[i] > m_max[i]) {
				throw Util::LastNotCommitException(
						"lasttranxnotcommit in rect::CheckConsistency, key is " + childKey
								+ ",  min is " + std::to_string(m_min[i]) + ", max is "
								+ std::to_string(m_max[i]));
			}
		}
	}
};

template<class DATATYPE, class ELEMTYPE, int NUMDIMS, int TMAXNODES>
struct Node;

/*
 * May be data or may be another subtree
 * The parents level determines this.
 * If the parents level is 0, then this is data
 */
template<class DATATYPE, class ELEMTYPE, int NUMDIMS, int TMAXNODES>
struct Branch {
	typedef Node<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES> node_type;
	typedef DTranxMempool<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES> mempool_type;
	typedef Rect<ELEMTYPE, NUMDIMS> rect_type;
	Branch()
			: m_child(NULL), childKey(Util::DTranxKeySpace::NOSUCHNODE) {
	}
	/*
	 * m_rect: Bounds
	 * m_child: Child node
	 * m_data: Data Id(non-negative)
	 */
	rect_type m_rect;
	std::string childKey;
	DATATYPE m_data;
	void serialize(BranchProto* branchProto) {
		m_rect.serialize(branchProto->mutable_rect());
		branchProto->set_child(childKey);
		m_data.serialize(branchProto->mutable_data());
	}

	void deserialize(const BranchProto& branchProto) {
		m_rect.deserialize(branchProto.rect());
		childKey = branchProto.child();
		m_data.deserialize(branchProto.data());
	}

	void CopyBranch(Branch& other) {
		other.childKey = childKey;
		m_rect.CopyRect(other.m_rect);
		m_data.CopyData(other.m_data);
	}

	bool operator==(const Branch &other) const {
		if (m_rect != other.m_rect) {
			return false;
		}
		if (childKey != other.childKey) {
			return false;
		}
		if (m_data != other.m_data) {
			return false;
		}
		return true;
	}
	bool operator!=(const Branch &other) const {
		if (m_rect != other.m_rect) {
			return true;
		}
		if (childKey != other.childKey) {
			return true;
		}
		if (m_data != other.m_data) {
			return true;
		}
		return false;
	}
	node_type* GetChild(mempool_type *mempool) {
		if (m_child == NULL && childKey != Util::DTranxKeySpace::NOSUCHNODE) {
			m_child = mempool->GetNode(childKey);
			if (m_child == NULL) {
				throw Util::LastNotCommitException("lasttranxnotcommit in node::GetChild1\n");
			}
			/*
			 * TODO: consistency check for m_level
			 */
			/*
			 * Check m_count, branches(rect)
			 */
			if (m_child->m_count > TMAXNODES || m_child->m_count < 0) {
				throw Util::LastNotCommitException("lasttranxnotcommit in node::GetChild2\n");
			}
			for (int i = 0; i < m_child->m_count; ++i) {
				m_child->m_branch[i].m_rect.CheckConsistency(childKey);
			}
		}
		return m_child;
	}
	void SetChild(node_type *child) {
		m_child = child;
		childKey = child->selfkey();
	}

	void ResetPointerKey() {
		m_child = NULL;
	}

	void Print(std::stringstream& ss) {
		ss << "childKey: " << childKey << "\n";
		m_rect.Print(ss);
		m_data.Print(ss);
	}
private:
	node_type* m_child;
};

/*
 * Node for each branch level
 */
template<class DATATYPE, class ELEMTYPE, int NUMDIMS, int TMAXNODES>
struct Node {
	typedef Branch<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES> branch_type;
	typedef Rect<ELEMTYPE, NUMDIMS> rect_type;

	bool IsInternalNode() {
		/*
		 * Not a leaf, but a internal node
		 */
		return (m_level > 0);
	}
	bool IsLeaf() {
		/*
		 * A leaf, contains data
		 */
		return (m_level == 0);
	}

	/*
	 * m_count: Count
	 * m_level: Leaf is zero, others positive
	 * m_branch: Branch
	 */
	int m_count;
	int m_level;
	branch_type m_branch[TMAXNODES];
	std::string selfKey;

	void set_selfkey(std::string selfKey) {
		this->selfKey = selfKey;
	}

	std::string selfkey() {
		return selfKey;
	}

	std::string serialize() {
		RTreeNodeProto rtreeNode;
		rtreeNode.set_count(m_count);
		rtreeNode.set_level(m_level);
		for (int i = 0; i < m_count; ++i) {
			m_branch[i].serialize(rtreeNode.add_branches());
		}
		return rtreeNode.SerializeAsString();
	}

	void deserialize(std::string content, std::string selfKey) {
		RTreeNodeProto rtreeNode;
		rtreeNode.Clear();
		rtreeNode.ParseFromString(content);
		m_count = rtreeNode.count();
		m_level = rtreeNode.level();
		for (int i = 0; i < m_count; ++i) {
			m_branch[i].deserialize(rtreeNode.branches(i));
		}
		this->selfKey = selfKey;
	}

	bool IsNodeDifferent(Node *other) {
		if (m_count != other->m_count) {
			return true;
		}
		if (m_level != other->m_level) {
			return true;
		}
		for (int i = 0; i < m_count; ++i) {
			if (m_branch[i] != other->m_branch[i]) {
				return true;
			}
		}
		if (selfKey != other->selfKey) {
			return true;
		}
		return false;
	}

	void CopyNode(Node *other) {
		other->m_count = m_count;
		other->m_level = m_level;
		other->selfKey = selfKey;
		for (int i = 0; i < m_count; ++i) {
			m_branch[i].CopyBranch(other->m_branch[i]);
		}
	}

	/*
	 * ResetPointerKey resets pointer key fields to NULL
	 * in order to detect readset.
	 */
	void ResetPointerKey() {
		for (int i = 0; i < m_count; ++i) {
			m_branch[i].ResetPointerKey();
		}
	}

	std::string Print() {
		std::stringstream ss;
		ss << Util::NODE_PRINT_START;
		ss << "selfkey: " << selfkey() << "\n";
		ss << "count: " << m_count << "\n";
		ss << "level: " << m_level << "\n";
		for (int i = 0; i < m_count; ++i) {
			m_branch[i].Print(ss);
		}
		ss << Util::NODE_PRINT_END;
		return ss.str();
	}
};

}
}
#endif /* RTREE_RTREE_NODE_H_ */
