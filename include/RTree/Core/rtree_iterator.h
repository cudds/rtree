/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef RTREE_RTREE_ITERATOR_H_
#define RTREE_RTREE_ITERATOR_H_

#include "rtree_node.h"
#include "rtree_common.h"

namespace RTree {
namespace Core {

template<class DATATYPE, class ELEMTYPE, int NUMDIMS, class ELEMTYPEREAL,
		int TMAXNODES, int TMINNODES>
class RTree;
/*
 * Important: Iterator is not remove safe.
 */
template<class DATATYPE, class ELEMTYPE, int NUMDIMS, class ELEMTYPEREAL,
		int TMAXNODES, int TMINNODES>
class Iterator {
	typedef Node<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES> node_type;
	typedef Branch<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES> branch_type;
	typedef DTranxMempool<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES> mempool_type;
	typedef Rect<ELEMTYPE, NUMDIMS> rect_type;
	typedef RTree<DATATYPE, ELEMTYPE, NUMDIMS, ELEMTYPEREAL, TMAXNODES,
			TMINNODES> rtree_type;
private:
	/*
	 * Max stack size. Allows almost n^32 where n is number of branches in node
	 */
	enum {
		MAX_STACK = 32
	};

	struct StackElement {
		node_type* m_node;
		int m_branchIndex;
	};

public:

	Iterator() {
		Init();
	}

	~Iterator() {
	}

	/*
	 * Is iterator invalid
	 */
	bool IsNull() const {
		return (m_tos <= 0);
	}

	/*
	 * Is iterator pointing to valid data
	 */
	bool IsNotNull() const {
		return (m_tos > 0);
	}

	/*
	 * Access the current data element. Caller must be sure iterator is not NULL first.
	 */
	DATATYPE& operator*() {
		ASSERT(IsNotNull());
		StackElement& curTos = m_stack[m_tos - 1];
		return curTos.m_node->m_branch[curTos.m_branchIndex].m_data;
	}

	/*
	 * Access the current data element. Caller must be sure iterator is not NULL first.
	 */
	const DATATYPE& operator*() const {
		ASSERT(IsNotNull());
		StackElement& curTos = m_stack[m_tos - 1];
		return curTos.m_node->m_branch[curTos.m_branchIndex].m_data;
	}

	/*
	 * Find the next data element
	 */
	bool Next(mempool_type *mempool) {
		return FindNextData(mempool);
	}

	/*
	 * Get the bounds for this node
	 */
	void GetBounds(ELEMTYPE a_min[NUMDIMS], ELEMTYPE a_max[NUMDIMS]) {
		ASSERT(IsNotNull());
		StackElement& curTos = m_stack[m_tos - 1];
		branch_type& curBranch = curTos.m_node->m_branch[curTos.m_branchIndex];

		for (int index = 0; index < NUMDIMS; ++index) {
			a_min[index] = curBranch.m_rect.m_min[index];
			a_max[index] = curBranch.m_rect.m_max[index];
		}
	}

private:
	/*
	 * Reset iterator
	 */
	void Init() {
		m_tos = 0;
	}

	/*
	 * Find the next data element in the tree (For internal use only)
	 */
	bool FindNextData(mempool_type *mempool) {
		for (;;) {
			if (m_tos <= 0) {
				return false;
			}
			/*
			 * Copy stack top cause it may change as we use it
			 */
			StackElement curTos = Pop();

			if (curTos.m_node->IsLeaf()) {
				/*
				 * Keep walking through data while we can
				 */
				if (curTos.m_branchIndex + 1 < curTos.m_node->m_count) {
					/*
					 * There is more data, just point to the next one
					 */
					Push(curTos.m_node, curTos.m_branchIndex + 1);
					return true;
				}
				/*
				 * No more data, so it will fall back to previous level
				 */
			} else {
				if (curTos.m_branchIndex + 1 < curTos.m_node->m_count) {
					/*
					 * Push sibling on for future tree walk
					 * This is the 'fall back' node when we finish with the current level
					 */
					Push(curTos.m_node, curTos.m_branchIndex + 1);
				}
				// Since cur node is not a leaf, push first of next level to get deeper into the tree
				node_type* nextLevelnode =
						curTos.m_node->m_branch[curTos.m_branchIndex].GetChild(mempool);
				Push(nextLevelnode, 0);

				/*
				 * If we pushed on a new leaf, exit as the data is ready at TOS
				 */
				if (nextLevelnode->IsLeaf()) {
					return true;
				}
			}
		}
	}

	/*
	 * Push node and branch onto iteration stack (For internal use only)
	 */
	void Push(node_type* a_node, int a_branchIndex) {
		m_stack[m_tos].m_node = a_node;
		m_stack[m_tos].m_branchIndex = a_branchIndex;
		++m_tos;
		ASSERT(m_tos <= MAX_STACK);
	}

	/*
	 * Pop element off iteration stack (For internal use only)
	 */
	StackElement& Pop() {
		ASSERT(m_tos > 0);
		--m_tos;
		return m_stack[m_tos];
	}

	/*
	 * Stack as we are doing iteration instead of recursion
	 * Top of Stack index
	 */
	StackElement m_stack[MAX_STACK];
	int m_tos;

	/*
	 * Allow hiding of non-public functions while allowing manipulation by logical owner
	 */
	friend rtree_type;
};

}
}
#endif /* RTREE_RTREE_ITERATOR_H_ */
