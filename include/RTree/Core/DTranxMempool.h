/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef RTREE_CORE_RTREE_MEMPOOL_H__
#define RTREE_CORE_RTREE_MEMPOOL_H__

#include <unordered_set>
#include "RTree/Util/DTranxHelper.h"
#include "RTree/Util/DebugLog.h"
#include "RTree/Util/DTranxKeySpace.h"

namespace RTree {
namespace Core {

template<class DATATYPE, class ELEMTYPE, int NUMDIMS, int TMAXNODES>
struct Node;

template<class DATATYPE, class ELEMTYPE, int NUMDIMS, int TMAXNODES>
class DTranxMempool {
	//MaybeLater: collect deleted keys
public:
	typedef Node<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES> node_type;
public:
	DTranxMempool(Util::DebugLog *debugLog, bool poolCached);
	~DTranxMempool() {
		if (dtranxHelper_ != NULL) {
			delete dtranxHelper_;
		}
		if (keySpace_ != NULL) {
			delete keySpace_;
		}
	}
	void InitDTranxForRTree(Util::DTranxHelper *dtranxHelper) {
		assert(dtranxHelper_ == NULL);
		dtranxHelper_ = dtranxHelper;
		keySpace_ = new Util::DTranxKeySpace(dtranxHelper_->ReadMeta());
	}
	node_type *GetNode(std::string key);
	node_type *GetRoot();
	/*
	 * AddWrite add node to the writeset during commit
	 * 	the first AddWrite only add new allocated nodes
	 * 	the second AddWrite adds deleted nodes
	 *
	 */
	void AddWrite(std::string key, node_type *newNode);
	void AddWrite(std::string key);

	std::string GetNextNewKey() {
		return keySpace_->GetNextNewKey();
	}
public:
	/*
	 * Node creation/deletion routines.
	 */
	node_type* new_node() {
		return new node_type();
	}
	node_type* new_leaf_node() {
		node_type* node = new node_type();
		node->set_selfkey(keySpace_->GetNextNewKey());
		AddWrite(node->selfkey(), node);
		return node;
	}
	node_type* new_leaf_root_node() {
		node_type* node = new node_type();
		node->set_selfkey(Util::DTranxKeySpace::ROOTNODEKEY);
		AddWrite(Util::DTranxKeySpace::ROOTNODEKEY, node);
		return node;
	}
	void delete_internal_node(node_type *node) {
		AddWrite(node->selfkey());
	}
	void delete_internal_root_node(node_type *node) {
		AddWrite(node->selfkey());
	}
	void delete_leaf_node(node_type *node) {
		AddWrite(node->selfkey());
	}

	/*
	 * Transaction interface
	 */

	void ClearNodeInPool(std::unordered_map<std::string, node_type*>& pool, std::string key) {
		if (pool.find(key) != pool.end() && pool[key] != NULL) {
			delete pool[key];
		}
		pool.erase(key);
	}

	void Clear(bool commitSucc) {
		if (isPoolCached) {
			if (commitSucc) {
				node_type *temp;
				for (auto it = swapSet.begin(); it != swapSet.end(); ++it) {
					//assert(nodesPool_.find(*it) != nodesPool_.end());
					std::string key1 = *it;
					std::string key2 = nodesPool_[*it]->selfkey();
					if (key1 == key2) {
						continue;
					}
					//assert(nodesPool_.find(key2) != nodesPool_.end());
					temp = nodesPool_[key1];
					nodesPool_[key1] = nodesPool_[key2];
					nodesPool_[key2] = temp;
					if (nodesPool_original_read.find(key1) == nodesPool_original_read.end()) {
						nodesPool_original_read[key1] = new_node();
					}
					nodesPool_[key1]->CopyNode(nodesPool_original_read[key1]);
					if (nodesPool_original_read.find(key2) == nodesPool_original_read.end()) {
						nodesPool_original_read[key2] = new_node();
					}
					nodesPool_[key2]->CopyNode(nodesPool_original_read[key2]);
				}
				for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
					if (swapSet.find(*it) != swapSet.end()) {
						continue;
					}
					//assert(nodesPool_.find(*it) != nodesPool_.end());
					if (nodesPool_original_read.find(*it) == nodesPool_original_read.end()) {
						/*
						 * special case, for new nodes, erase the cache, because
						 * if any concurrent transaction updated this new node,
						 * this outdated in-mem version is used but check will succeed since
						 * DDSBrick cache has the right version number.
						 */
						if (nodesPool_[*it] != NULL) {
							delete nodesPool_[*it];
							nodesPool_.erase(*it);
						}
						continue;

					}
					nodesPool_[*it]->CopyNode(nodesPool_original_read[*it]);
					nodesPool_[*it]->ResetPointerKey();
					nodesPool_original_read[*it]->ResetPointerKey();
				}
				for (auto it = updateSet.begin(); it != updateSet.end(); ++it) {
					//assert(nodesPool_.find(*it) != nodesPool_.end());
					//assert(nodesPool_original_read.find(*it) != nodesPool_original_read.end());
					nodesPool_[*it]->CopyNode(nodesPool_original_read[*it]);
					nodesPool_[*it]->ResetPointerKey();
					nodesPool_original_read[*it]->ResetPointerKey();
				}
				for (auto it = readSet.begin(); it != readSet.end(); ++it) {
					nodesPool_[*it]->ResetPointerKey();
					nodesPool_original_read[*it]->ResetPointerKey();
				}
				for (auto it = swapSet.begin(); it != swapSet.end(); ++it) {
					nodesPool_[*it]->ResetPointerKey();
					nodesPool_original_read[*it]->ResetPointerKey();
				}
			} else {
				for (auto it = readSet.begin(); it != readSet.end(); ++it) {
					ClearNodeInPool(nodesPool_, *it);
					ClearNodeInPool(nodesPool_original_read, *it);
				}
				for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
					ClearNodeInPool(nodesPool_, *it);
					ClearNodeInPool(nodesPool_original_read, *it);
				}
				for (auto it = swapSet.begin(); it != swapSet.end(); ++it) {
					ClearNodeInPool(nodesPool_, *it);
					ClearNodeInPool(nodesPool_original_read, *it);
				}
				for (auto it = updateSet.begin(); it != updateSet.end(); ++it) {
					ClearNodeInPool(nodesPool_, *it);
					ClearNodeInPool(nodesPool_original_read, *it);
				}
			}
			readSet.clear();
			writeSet.clear();
			swapSet.clear();
			updateSet.clear();
		} else {
			for (auto it = nodesPool_.begin(); it != nodesPool_.end(); ++it) {
				if (it->second != NULL) {
					delete it->second;
				}
			}
			for (auto it = nodesPool_original_read.begin(); it != nodesPool_original_read.end();
					++it) {
				if (it->second != NULL) {
					delete it->second;
				}
			}
			nodesPool_.clear();
			nodesPool_original_read.clear();
			readSet.clear();
			writeSet.clear();
			swapSet.clear();
			updateSet.clear();
		}
	}

	bool Commit();

	void Abort(std::string message) {
		debugLog->Write(Util::ABORT_PRINT_START);
		debugLog->Write(message);
		debugLog->Write(Util::ABORT_PRINT_END);
		dtranxHelper_->Abort();
		Clear(false);
	}

private:
	Util::DTranxHelper *dtranxHelper_;
	/*
	 * nodesPool_* is a memory pool that stores all de-serialized node objects
	 * specifically nodesPool_ is the one that might be changed during the transaction
	 * while nodesPool_original_read only records the version that is read in
	 * the first place. nodesPool_original_read is used to detect writeset
	 * during transaction commit
	 *
	 * writeSet: new/delete nodes, might include update nodes
	 *		e.g. when new root is created, old root is automatically added to writeSet by AddWrite func.
	 * swapSet: swapped nodes
	 * updateSet: updated pre-existing nodes
	 * readSet: read items
	 */
	std::unordered_map<std::string, node_type *> nodesPool_;
	std::unordered_map<std::string, node_type *> nodesPool_original_read;
	std::unordered_set<std::string> writeSet;
	std::unordered_set<std::string> swapSet;
	std::unordered_set<std::string> updateSet;
	std::unordered_set<std::string> readSet;

	bool isPoolCached;

	/*
	 * keySpace helps to generate next new key
	 */
	Util::DTranxKeySpace *keySpace_;

	Util::DebugLog *debugLog;
};

template<class DATATYPE, class ELEMTYPE, int NUMDIMS, int TMAXNODES>
DTranxMempool<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES>::DTranxMempool(Util::DebugLog *debugLog, bool poolCached)
		: dtranxHelper_(NULL), keySpace_(NULL), debugLog(debugLog), nodesPool_(), isPoolCached(poolCached) {
	std::cout << "rtree_mempool constructor" << std::endl;
	keySpace_ = new Util::DTranxKeySpace(1);
}

template<class DATATYPE, class ELEMTYPE, int NUMDIMS, int TMAXNODES>
Node<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES> *DTranxMempool<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES>::GetRoot() {
	if (nodesPool_.find(Util::DTranxKeySpace::ROOTNODEKEY) != nodesPool_.end()) {
		if (readSet.find(Util::DTranxKeySpace::ROOTNODEKEY) == readSet.end()) {
			readSet.insert(Util::DTranxKeySpace::ROOTNODEKEY);
			dtranxHelper_->ReadNode(Util::DTranxKeySpace::ROOTNODEKEY);
		}
		return nodesPool_[Util::DTranxKeySpace::ROOTNODEKEY];
	}
	std::string content = dtranxHelper_->ReadNode(Util::DTranxKeySpace::ROOTNODEKEY);
	if (content == Util::DTranxKeySpace::NOSUCHNODE) {
		return NULL;
	}
	readSet.insert(Util::DTranxKeySpace::ROOTNODEKEY);
	node_type *p = new_node();
	node_type *q = new_node();
	p->deserialize(content, Util::DTranxKeySpace::ROOTNODEKEY);
	nodesPool_[Util::DTranxKeySpace::ROOTNODEKEY] = p;
	p->CopyNode(q);
	nodesPool_original_read[Util::DTranxKeySpace::ROOTNODEKEY] = q;
	if (debugLog->isDebugEnabled()) {
		debugLog->Write(p->Print());
	}
	return p;
}

template<class DATATYPE, class ELEMTYPE, int NUMDIMS, int TMAXNODES>
Node<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES> *DTranxMempool<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES>::GetNode(
		std::string key) {
	if (nodesPool_.find(key) != nodesPool_.end()) {
		if (readSet.find(key) == readSet.end()) {
			readSet.insert(key);
			dtranxHelper_->ReadNode(key);
		}
		return nodesPool_[key];
	}
	std::string content = dtranxHelper_->ReadNode(key);
	if (content == Util::DTranxKeySpace::NOSUCHNODE) {
		return NULL;
	}
	readSet.insert(key);
	node_type *p = new_node();
	node_type *q = new_node();
	p->deserialize(content, key);
	nodesPool_[key] = p;
	p->CopyNode(q);
	nodesPool_original_read[key] = q;
	if (debugLog->isDebugEnabled()) {
		debugLog->Write(p->Print());
	}
	return p;
}

template<class DATATYPE, class ELEMTYPE, int NUMDIMS, int TMAXNODES>
void DTranxMempool<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES>::AddWrite(std::string key,
		node_type *newNode) {
	/*
	 * it might already be in the pool
	 * eg. new root is created
	 * make sure the previous node is already configured with another new key
	 * besides, that new key should be generated recently and not in the dtranx db yet.
	 */
	if (nodesPool_.find(key) != nodesPool_.end()) {
		//assert(key != nodesPool_[key]->selfkey());
		std::string newKey = nodesPool_[key]->selfkey();
		//assert(nodesPool_.find(newKey) == nodesPool_.end());
		nodesPool_[newKey] = nodesPool_[key];
		writeSet.insert(newKey);
		nodesPool_.erase(key);
	}
	nodesPool_[key] = newNode;
	writeSet.insert(key);
}

template<class DATATYPE, class ELEMTYPE, int NUMDIMS, int TMAXNODES>
void DTranxMempool<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES>::AddWrite(std::string key) {
	//assert(nodesPool_.find(key) != nodesPool_.end());
	writeSet.insert(key);
}

template<class DATATYPE, class ELEMTYPE, int NUMDIMS, int TMAXNODES>
bool DTranxMempool<DATATYPE, ELEMTYPE, NUMDIMS, TMAXNODES>::Commit() {
	bool readTranx = true;
	debugLog->Write(Util::COMMIT_PRINT_START);
	/*
	 * Find all the swapping
	 */
	for (auto it = readSet.begin(); it != readSet.end(); ++it) {
		if (*it != nodesPool_[*it]->selfkey()) {
			swapSet.insert(nodesPool_[*it]->selfkey());
			readTranx = false;
			dtranxHelper_->WriteNode(nodesPool_[*it]->selfkey(), nodesPool_[*it]->serialize());
			if (debugLog->isDebugEnabled()) {
				debugLog->Write(nodesPool_[*it]->Print());
			}
		}
	}
	/*
	 * Add new allocated nodes to writeset
	 */
	for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
		readTranx = false;
		dtranxHelper_->WriteNode(*it, nodesPool_[*it]->serialize());
		if (debugLog->isDebugEnabled()) {
			debugLog->Write(nodesPool_[*it]->Print());
		}
	}
	/*
	 * Automatically detect any modified nodes
	 */
	for (auto it = readSet.begin(); it != readSet.end(); ++it) {
		if (writeSet.find(*it) != writeSet.end()) {
			continue;
		}
		if (swapSet.find(*it) != swapSet.end()) {
			continue;
		}
		//assert(nodesPool_original_read.find(*it) != nodesPool_original_read.end());
		if (nodesPool_[*it]->IsNodeDifferent(nodesPool_original_read[*it])) {
			updateSet.insert(*it);
			readTranx = false;
			dtranxHelper_->WriteNode(*it, nodesPool_[*it]->serialize());
			if (debugLog->isDebugEnabled()) {
				debugLog->Write(nodesPool_[*it]->Print());
			}
		}
	}
	debugLog->Write(Util::COMMIT_PRINT_END);
	bool result = dtranxHelper_->Commit();
	Clear(result);
	return result;
}

}  // namespace Core
}  // namespace RTree

#endif
