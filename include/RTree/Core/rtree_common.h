/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef RTREE_RTREE_COMMON_H_
#define RTREE_RTREE_COMMON_H_

namespace RTree{
namespace Core{

/*
 * RTree uses ASSERT( condition )
 */
#define ASSERT assert
#ifndef MIN
  #define MIN std::min
#endif
#ifndef MAX
  #define MAX std::max
#endif

template<class T, class B> struct Derived_from {
    static void constraints(T p) { B pb = p; }
    Derived_from() { void(*p)(T) = constraints; }
};

}
}
#endif /* RTREE_RTREE_COMMON_H_ */
