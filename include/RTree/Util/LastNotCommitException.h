/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef RTREE_UTIL_LASTNOTCOMMITEXCEPTION_H_
#define RTREE_UTIL_LASTNOTCOMMITEXCEPTION_H_

#include <exception>
#include <string>
namespace RTree {
namespace Util {

class LastNotCommitException: public std::exception {
public:
	LastNotCommitException(std::string m) :
			msg(m) {
	}
	~LastNotCommitException() throw () {
	}
	const char* what() const throw () {
		return msg.c_str();
	}

private:
	std::string msg;
};

} /* namespace Util */
} /* namespace RTree */

#endif /* RTREE_UTIL_LASTNOTCOMMITEXCEPTION_H_ */
