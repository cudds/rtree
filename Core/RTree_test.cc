/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "RTree/Core/RTree.h"

using namespace RTree;
typedef int ValueType;
typedef Core::RTree<ValueType, int, 2, float> MyTree;
