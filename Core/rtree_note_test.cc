/*
 * rtree_note_test.cc
 *
 *  Created on: Feb 27, 2017
 *      Author: neal
 */

#include "gtest/gtest.h"
#include "RTree/Core/rtree_node.h"

using namespace RTree;

TEST(RTREENODE, serialization) {
	typedef Core::Node<Core::StringDataType, float, 3, 8> NodeType;
	typedef NodeType::branch_type BranchType;
	typedef NodeType::rect_type RectType;
	NodeType node;
	node.selfKey = "key1";
	node.m_count = 2;
	node.m_level = 3;
	BranchType branch;
	branch.childKey = "key2";
	branch.m_data = "data2";
	branch.m_rect.m_min[0] = 1;
	branch.m_rect.m_min[1] = 1;
	branch.m_rect.m_min[2] = 1;
	branch.m_rect.m_max[0] = 2;
	branch.m_rect.m_max[1] = 2;
	branch.m_rect.m_max[2] = 2;
	node.m_branch[0] = branch;
	branch.childKey = "key3";
	branch.m_data = "data3";
	branch.m_rect.m_min[0] = 3;
	branch.m_rect.m_min[1] = 3;
	branch.m_rect.m_min[2] = 3;
	branch.m_rect.m_max[0] = 4;
	branch.m_rect.m_max[1] = 4;
	branch.m_rect.m_max[2] = 4;
	node.m_branch[1] = branch;
	std::string nodeStr = node.serialize();

	NodeType node2;
	node2.deserialize(nodeStr, "key1");
	EXPECT_EQ("key1", node2.selfKey);
	EXPECT_EQ(2, node2.m_count);
	EXPECT_EQ(3, node2.m_level);
	Core::StringDataType data("data2");
	EXPECT_EQ(data, node2.m_branch[0].m_data);
	EXPECT_EQ("key2", node2.m_branch[0].childKey);
	EXPECT_EQ(1, node2.m_branch[0].m_rect.m_min[2]);
	data.strData = "data3";
	EXPECT_EQ(data, node2.m_branch[1].m_data);
	EXPECT_EQ("key3", node2.m_branch[1].childKey);
	EXPECT_EQ(4, node2.m_branch[1].m_rect.m_max[2]);
}

TEST(RTREENODE, Equal) {
	typedef Core::Node<Core::StringDataType, float, 3, 8> NodeType;
	typedef NodeType::branch_type BranchType;
	typedef NodeType::rect_type RectType;
	Core::StringDataType data1("data2");
	Core::StringDataType data2("data2");
	EXPECT_EQ(data1, data2);
	data2.strData = "data3";
	EXPECT_NE(data1, data2);

	BranchType branch;
	branch.childKey = "key2";
	branch.m_data = "data2";
	branch.m_rect.m_min[0] = 1;
	branch.m_rect.m_min[1] = 1;
	branch.m_rect.m_min[2] = 1;
	branch.m_rect.m_max[0] = 2;
	branch.m_rect.m_max[1] = 2;
	branch.m_rect.m_max[2] = 2;
	BranchType branch2;
	branch2.childKey = "key2";
	branch2.m_data = "data2";
	branch2.m_rect.m_min[0] = 1;
	branch2.m_rect.m_min[1] = 1;
	branch2.m_rect.m_min[2] = 1;
	branch2.m_rect.m_max[0] = 2;
	branch2.m_rect.m_max[1] = 2;
	branch2.m_rect.m_max[2] = 2;
	EXPECT_EQ(branch, branch2);
	branch2.childKey = "key3";
	EXPECT_NE(branch, branch2);

	RectType rect;
	rect.m_min[0] = 1;
	rect.m_min[1] = 1;
	rect.m_min[2] = 1;
	rect.m_max[0] = 2;
	rect.m_max[1] = 2;
	rect.m_max[2] = 2;
	RectType rect2;
	rect2.m_min[0] = 1;
	rect2.m_min[1] = 1;
	rect2.m_min[2] = 1;
	rect2.m_max[0] = 2;
	rect2.m_max[1] = 2;
	rect2.m_max[2] = 2;
	EXPECT_EQ(rect, rect2);
	rect2.m_min[0] = 3;
	EXPECT_NE(rect, rect2);

	NodeType node;
	node.selfKey = "key1";
	node.m_count = 2;
	node.m_level = 3;
	branch.childKey = "key2";
	branch.m_data = "data2";
	branch.m_rect.m_min[0] = 1;
	branch.m_rect.m_min[1] = 1;
	branch.m_rect.m_min[2] = 1;
	branch.m_rect.m_max[0] = 2;
	branch.m_rect.m_max[1] = 2;
	branch.m_rect.m_max[2] = 2;
	node.m_branch[0] = branch;
	branch.childKey = "key3";
	branch.m_data = "data3";
	branch.m_rect.m_min[0] = 3;
	branch.m_rect.m_min[1] = 3;
	branch.m_rect.m_min[2] = 3;
	branch.m_rect.m_max[0] = 4;
	branch.m_rect.m_max[1] = 4;
	branch.m_rect.m_max[2] = 4;
	node.m_branch[1] = branch;
	NodeType node2;
	node2.selfKey = "key1";
	node2.m_count = 2;
	node2.m_level = 3;
	branch.childKey = "key4";
	branch.m_data = "data2";
	branch.m_rect.m_min[0] = 1;
	branch.m_rect.m_min[1] = 1;
	branch.m_rect.m_min[2] = 1;
	branch.m_rect.m_max[0] = 2;
	branch.m_rect.m_max[1] = 2;
	branch.m_rect.m_max[2] = 2;
	node2.m_branch[0] = branch;
	branch.childKey = "key3";
	branch.m_data = "data3";
	branch.m_rect.m_min[0] = 3;
	branch.m_rect.m_min[1] = 3;
	branch.m_rect.m_min[2] = 3;
	branch.m_rect.m_max[0] = 4;
	branch.m_rect.m_max[1] = 4;
	branch.m_rect.m_max[2] = 4;
	node2.m_branch[1] = branch;
	EXPECT_TRUE(node.IsNodeDifferent(&node2));
	node2.m_branch[0].childKey = "key2";
	EXPECT_FALSE(node.IsNodeDifferent(&node2));
}

TEST(RTREENODE, CopyNode) {
	typedef Core::Node<Core::StringDataType, float, 3, 8> NodeType;
	typedef NodeType::branch_type BranchType;
	typedef NodeType::rect_type RectType;
	NodeType node;
	node.selfKey = "key1";
	node.m_count = 2;
	node.m_level = 3;
	BranchType branch;
	branch.childKey = "key2";
	branch.m_data = "data2";
	branch.m_rect.m_min[0] = 1;
	branch.m_rect.m_min[1] = 1;
	branch.m_rect.m_min[2] = 1;
	branch.m_rect.m_max[0] = 2;
	branch.m_rect.m_max[1] = 2;
	branch.m_rect.m_max[2] = 2;
	node.m_branch[0] = branch;
	branch.childKey = "key3";
	branch.m_data = "data3";
	branch.m_rect.m_min[0] = 3;
	branch.m_rect.m_min[1] = 3;
	branch.m_rect.m_min[2] = 3;
	branch.m_rect.m_max[0] = 4;
	branch.m_rect.m_max[1] = 4;
	branch.m_rect.m_max[2] = 4;
	node.m_branch[1] = branch;
	NodeType node2;
	node.CopyNode(&node2);
	EXPECT_EQ("key1", node2.selfKey);
	EXPECT_EQ(2, node2.m_count);
	EXPECT_EQ(3, node2.m_level);
	Core::StringDataType data("data2");
	EXPECT_EQ(data, node2.m_branch[0].m_data);
	EXPECT_EQ("key2", node2.m_branch[0].childKey);
	EXPECT_EQ(1, node2.m_branch[0].m_rect.m_min[2]);
	data.strData = "data3";
	EXPECT_EQ(data, node2.m_branch[1].m_data);
	EXPECT_EQ("key3", node2.m_branch[1].childKey);
	EXPECT_EQ(4, node2.m_branch[1].m_rect.m_max[2]);
}

TEST(RTREENODE, PrintNode) {
	typedef Core::Node<Core::StringDataType, float, 3, 8> NodeType;
	typedef NodeType::branch_type BranchType;
	typedef NodeType::rect_type RectType;
	NodeType node;
	node.selfKey = "key1";
	node.m_count = 2;
	node.m_level = 3;
	BranchType branch;
	branch.childKey = "key2";
	branch.m_data = "data2";
	branch.m_rect.m_min[0] = 1;
	branch.m_rect.m_min[1] = 1;
	branch.m_rect.m_min[2] = 1;
	branch.m_rect.m_max[0] = 2;
	branch.m_rect.m_max[1] = 2;
	branch.m_rect.m_max[2] = 2;
	node.m_branch[0] = branch;
	branch.childKey = "key3";
	branch.m_data = "data3";
	branch.m_rect.m_min[0] = 3;
	branch.m_rect.m_min[1] = 3;
	branch.m_rect.m_min[2] = 3;
	branch.m_rect.m_max[0] = 4;
	branch.m_rect.m_max[1] = 4;
	branch.m_rect.m_max[2] = 4;
	node.m_branch[1] = branch;
	std::cout << node.Print() << std::endl;
}
