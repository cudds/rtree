#!/usr/bin/env python
'''
	install header files and library
'''

import shutil
import subprocess

def runBash(command):
	out, err = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).communicate()
	return out
	
rtreeBash = "\
	protoc -I. --cpp_out=./include/RTree Core/RTreeNode.proto;\
	rm ./include/RTree/Core/RTreeNode.pb.cc;"
runBash(rtreeBash)
try:
	shutil.rmtree("/usr/local/include/RTree")
except OSError:
	print "first time installing RTree on this computer"
shutil.copytree("include/RTree", "/usr/local/include/RTree")
shutil.copyfile("Build/librtree.a","/usr/local/lib/librtree.a")
shutil.copyfile("Build/librtree.so","/usr/local/lib/librtree.so")
